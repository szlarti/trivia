CXXFLAGS += -std=c++17 -Wall -Wextra -Werror

OUT_DIR = out
BIN = $(OUT_DIR)/trivia
OBJS = GameRunner.o Game.o Category.o Player.o


.PHONY: build run clean

build: $(BIN)
	@echo "Build ok"

run: build
	@echo "Run $(BIN):"
	@$(BIN)

clean:
	@echo "Clean ($(OUT_DIR))"
	@rm -f *.o
	@rm -rf $(OUT_DIR)


$(OUT_DIR):
	mkdir $@

Player.o: Player.h
Category.o: Category.h
Game.o: Game.h Category.h Player.h
GameRunner.o: Game.h Category.h Player.h

$(BIN): $(OBJS) $(OUT_DIR)
	$(CXX) $(OBJS) -o $@

