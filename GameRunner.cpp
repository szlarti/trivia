﻿#include "Game.h"

int main()
{

	Game aGame;

	aGame.addPlayer("Chet");
	aGame.addPlayer("Pat");
	aGame.addPlayer("Sue");

	if (aGame.isPlayable())
	{
		while ( aGame.nextRoll() );
	}

}

