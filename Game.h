#ifndef GAME_H_
#define GAME_H_

#include <string>
#include <vector>
#include "Category.h"
#include "Player.h"

class Game
{

	private:
		std::vector<Player> players;
		std::vector<Player>::iterator currentPlayer = players.end();
		Category popCategory, scienceCategory, sportsCategory, rockCategory;

		void askQuestion();
		Category& currentCategory();
		bool notAWinner() const ;
		void nextPlayer();
		bool checkAnswer(const bool answer);

	public:
		Game();
		Game(const Game&) = delete;
		Game(Game&&) = delete;
		~Game() = default;
		Game& operator=(const Game&) = delete;
		Game& operator=(Game&&) = delete;
		bool isPlayable() const;
		void addPlayer(const std::string& playerName);
		bool nextRoll();

};

#endif /* GAME_H_ */

