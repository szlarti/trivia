#include <string>
#include <iostream>
#include <stdlib.h>
#include "Player.h"

using std::string, std::cout, std::endl;

static const int boardSize = 12;
static const int correctAnswerProbability = 9;

Player::Player(const string& playerName) : name(playerName)
{
	srand(time(nullptr));
}

const string& Player::getName() const
{
	return name;
}

int Player::getPlace() const
{
	return place;
}

int Player::getCoins() const
{
	return purse;
}

bool Player::isInPenaltyBox() const
{
	return inPenaltyBox;
}

int Player::roll() const
{
	return rand() % 6 + 1;
}

void Player::move(const int steps)
{
	place = (place + steps) % boardSize;
	cout << name << "'s new location is " << place << endl;
}

bool Player::answer() const
{
	return (rand() % correctAnswerProbability != 0);
}

void Player::addCoin()
{
	++purse;
}

void Player::sendToPenaltyBox()
{
	inPenaltyBox = true;
}

void Player::getOutOfPenaltyBox()
{
	inPenaltyBox = false;
}

