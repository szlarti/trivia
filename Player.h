#ifndef PLAYER_H
#define PLAYER_H

#include <string>

class Player
{
	private:
		const std::string name;
		int place = 0;
		int purse = 0;
		bool inPenaltyBox = false;
	public:
		Player(const std::string& playerName);
		Player(const Player&) = delete;
		Player(Player&&) = default;
		~Player() = default;
		Player& operator=(const Player&) = delete;
		Player& operator=(Player&&) = delete;
		const std::string& getName() const;
		int getPlace() const;
		int getCoins() const;
		bool isInPenaltyBox() const;
		int roll() const;
		void move(const int steps);
		bool answer() const;
		void addCoin();
		void sendToPenaltyBox();
		void getOutOfPenaltyBox();
};

#endif //PLAYER_H

