﻿#include "Game.h"
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <algorithm>

using std::string, std::cout, std::endl;
using std::vector;

static const int numberOfQuestions = 50;
static const int coinsToWin = 6;

Game::Game():
	popCategory(numberOfQuestions, "Pop"),
	scienceCategory(numberOfQuestions, "Science"),
	sportsCategory(numberOfQuestions, "Sports"),
	rockCategory(numberOfQuestions, "Rock")
{}

bool Game::isPlayable() const
{
	return players.size() >= 2;
}

void Game::addPlayer(const string& playerName)
{
	players.push_back(Player(playerName));
	currentPlayer = --players.end();
	cout << playerName << " was added" << endl;
	cout << "They are player number " << players.size() << endl;
}

bool Game::nextRoll()
{
	nextPlayer();
	const string& playerName = currentPlayer->getName();
	const int roll = currentPlayer->roll();
	cout << playerName << " has rolled a " << roll << endl;
	if (currentPlayer->isInPenaltyBox())
	{
		if (roll % 2 != 0)
		{
			currentPlayer->getOutOfPenaltyBox();
			cout << playerName << " is getting out of the penalty box" << endl;
		}
		else
		{
			cout << playerName << " is not getting out of the penalty box" << endl;
			return true;
		}
	}
	currentPlayer->move(roll);
	try
	{
		askQuestion();
		return checkAnswer(currentPlayer->answer());
	}
	catch (OutOfQuestions& e)
	{
		cout << "No more questions in " << currentCategory().getName() << " category!" << endl;
		auto maxIt = std::max_element(players.begin(), players.end(), [](const Player& p1, const Player& p2){ return p1.getCoins() < p2.getCoins(); });
		cout << maxIt->getName() << " won the game with " << maxIt->getCoins() << " Gold Coins" << endl;
		return false;
	}
}

void Game::askQuestion()
{
		cout << currentCategory().getNextQuestion() << endl;
}

Category& Game::currentCategory()
{
	std::array categories = { &popCategory, &scienceCategory, &sportsCategory, &rockCategory };
	return *categories[currentPlayer->getPlace() % 4];
}

void Game::nextPlayer()
{
	++currentPlayer;
	if (currentPlayer == players.end())
	{
		currentPlayer = players.begin();
	}
	cout << endl << currentPlayer->getName() << " is the current player" << endl;
}

bool Game::checkAnswer(const bool answer)
{
	if (answer)
	{
		cout << "Answer was corrent!" << endl;
		currentPlayer->addCoin();
		cout << currentPlayer->getName() << " now has " << currentPlayer->getCoins() << " Gold Coins." << endl;
		if (notAWinner())
		{
			return true;
		}
		else
		{
			cout << "Winner!!!" << endl;
			return false;
		}
	}
	else
	{
		cout << "Question was incorrectly answered" << endl;
		cout << currentPlayer->getName() + " was sent to the penalty box" << endl;
		currentPlayer->sendToPenaltyBox();
		return true;
	}
}

bool Game::notAWinner() const
{
	return currentPlayer->getCoins() < coinsToWin;
}

