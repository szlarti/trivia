#include "Category.h"

Category::Category(const int numberOfQuestions, const std::string categoryName) : name(categoryName)
{
	for (int i = 0; i < numberOfQuestions; i++)
	{
		questions.push_back(name + " Question " + std::to_string(i+1));
	}
}

const std::string& Category::getName() const
{
	return name;
}

const std::string& Category::getNextQuestion()
{
	if (indexOfNextQuestion < questions.size())
	{
		return questions[indexOfNextQuestion++];
	}
	else
	{
		throw OutOfQuestions();
	}
}

