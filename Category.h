#ifndef CATEGORY_H
#define CATEGORY_H

#include <exception>
#include <string>
#include <vector>

class OutOfQuestions: public std::exception {};

class Category
{
	public:
		Category(const int numberOfQuestions, const std::string categoryName);
		Category(const Category&) = delete;
		Category(Category&&) = delete;
		~Category() = default;
		Category& operator=(const Category&) = delete;
		Category& operator=(Category&&) = delete;
		const std::string& getName() const;
		const std::string& getNextQuestion();
	private:
		const std::string name;
		unsigned int indexOfNextQuestion = 0;
		std::vector<std::string> questions = {};
};

#endif //CATEGORY_H

